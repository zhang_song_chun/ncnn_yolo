# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/liu/workspace/YK/ncnn_yolo_test/demo_resnet.cpp" "/home/liu/workspace/YK/ncnn_yolo_test/cmake-build-release/CMakeFiles/ncnn_yolo_test.dir/demo_resnet.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/liu/workspace/YK/ncnn-master/build/install/include/ncnn"
  "/home/liu/lib/install/opencv/usr/local/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
