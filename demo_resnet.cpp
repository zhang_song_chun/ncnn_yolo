//
// Created by liu on 21-2-21.
//


#include <stdio.h>
#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <fstream>

#include "platform.h"
#include "net.h"

using namespace std;
using namespace cv;


vector<string> split(const string& str, const string& pattern) {
    vector<string> ret;
    if (pattern.empty()) return ret;
    size_t start = 0, index = str.find_first_of(pattern, 0);
    while (index != str.npos) {
        if (start != index)
            ret.push_back(str.substr(start, index - start));
        start = index + 1;
        index = str.find_first_of(pattern, start);
    }
    if (!str.substr(start).empty())
        ret.push_back(str.substr(start));
    return ret;
}

void get_label(vector<string> &labels,const char* file_path){
    FILE *fp = fopen(file_path,"r");
    ifstream infile;
    string temp;
    const char*d = ":";
    infile.open("../data/label.txt");
    while(getline(infile,temp))
    {
        vector<string> line = split(temp,":");
        labels.push_back(line[1]);
    }
}

static int print_topk(const std::vector<float>& cls_scores, int topk,vector<string> labels)
{
    // partial sort topk with index
    int size = cls_scores.size();
    std::vector< std::pair<float, int> > vec;
    vec.resize(size);
    for (int i=0; i<size; i++)
    {
        vec[i] = std::make_pair(cls_scores[i], i);
    }

    std::partial_sort(vec.begin(), vec.begin() + topk, vec.end(),
                      std::greater< std::pair<float, int> >());

    // print topk and score
    for (int i=0; i<topk; i++)
    {
        float score = vec[i].first;
        int index = vec[i].second;
        string label = labels[index];
        fprintf(stderr, "%d = %f  %s \n", index, score ,label.c_str());

    }

    return 0;
}

int main(){

    vector<string> labels;
    get_label(labels,"../data/label.txt");

    cv::Mat bgr = cv::imread("../data/test.jpg");

    int img_w = bgr.cols;
    int img_h = bgr.rows;

    ncnn::Mat in = ncnn::Mat::from_pixels_resize(bgr.data, ncnn::Mat::PIXEL_BGR, bgr.cols, bgr.rows, 224, 224);
    const float mean_vals[3] = {104.0f,117.0f,123.0f};
    const float norm_vals[3] = {0.007843f, 0.007843f, 0.007843f};
    in.substract_mean_normalize(mean_vals,norm_vals);

    ncnn::Net resnet18;
    resnet18.load_param("../model/resnet.param");
    resnet18.load_model("../model/resnet.bin");
    ncnn::Extractor ex = resnet18.create_extractor();
    ex.set_num_threads(4);
    ex.input("data",in);
    ncnn::Mat out;
    ex.extract("resnetv22_dense0_fwd",out);

    vector<float> output;
    output.resize(out.w);
    for (int j=0; j<out.w; j++)
    {
        output[j] = out[j];
    }
    print_topk(output,2,labels);





}
